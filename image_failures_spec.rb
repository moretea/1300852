feature "Image failures" do
  background do
    @category = Category.make 
    @product_ok = Product.make(:category => @category)
    ensure_test_s3_files! 
   
    @product_not_ok = Product.make(:category => @category)
    Product.stub(:search).and_return(MockSphinx::ProductStubber.new(Product.all))
  end 
    
  it "should create a log", :js => true do
    visit shop_category_path(@category)
   
    ImageFailure.count.should == 1
  end    
end